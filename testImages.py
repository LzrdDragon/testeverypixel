import datetime
import os.path
from random import randint

from PIL import Image, ImageFilter, ImageDraw, ImageFont


# thumbnail, по-моему, меняет непосредственно картинку, а resize копирует её и меняет, то есть,
# если использовать resize, нужно будет возвращать картинку. Плюс ко всему они по-разному меняют размер.
# thumbnail должен как минимум сохранят соотношение сторон сам по себе и менять, соответственно,
# не совсем по точно заданным размерам
def resizing(image: Image.Image, new_height: int = 0, new_width: int = 0, image_class=Image) -> bool:
    """
    Attribute image has to be an object received, as a result of Image.open() of PIL Image class

    Attribute image_class should be directly an Image class of PIL module

        It returns True if everything is alright or prints an error and returns False if there was an exception
        due running
    """
    print('resizing')
    try:
        width, height = image.size
        if new_height and new_width:
            image.thumbnail((new_width, new_height), image_class.ANTIALIAS)
        elif new_height:
            ratio = float(new_height) / float(height)
            calculated_width = int(float(width) * ratio)
            image.thumbnail((calculated_width, new_height), image_class.ANTIALIAS)
        elif new_width:
            ratio = float(new_width) / float(width)
            calculated_height = int(float(height) * ratio)
            image.thumbnail((new_width, calculated_height), image_class.ANTIALIAS)
    except Exception as err:
        print(f'There is an Error in resizing function:\n{err}')
        return False
    return True


def affecting(image: Image.Image, filter_class=ImageFilter) -> Image.Image:
    print('affecting')
    image = image.filter(filter_class.BLUR)
    return image


def date_drawing(image: Image.Image, draw_class=ImageDraw, font_class=ImageFont) -> None:
    print('date_drawing')
    # Создаём draw объект
    drawing_object = draw_class.Draw(image)

    # Создаём объекты надписи и шрифта
    date = str(datetime.date.today())
    font = font_class.truetype('arial.ttf', 22)

    # Позиционируем текст
    text_width, text_height = drawing_object.textsize(date, font)
    width, height = image.size
    x = width / 2 - text_width / 2
    y = height / 1.2 - text_height

    # Наносим надпись на изображение через draw объект
    drawing_object.text((x, y), date, font=font)


def watermarking(image: Image.Image, draw_class=ImageDraw, font_class=ImageFont, image_class=Image) -> Image.Image:
    print('watermarking')
    # Создаём новый слой под текст
    layer_to_draw = image_class.new('RGBA', image.size, (255, 255, 255, 0))

    # Создаём текст
    watermark_text = "Test for Everypixel group"
    font = font_class.truetype("arial.ttf", 18)

    # Создаём Draw объект
    drawing_object = draw_class.Draw(layer_to_draw)

    # Позиционируем текст
    width, height = image.size
    text_width, text_height = drawing_object.textsize(watermark_text, font)
    x = width / 2 - text_width / 2
    y = height / 1.4 - text_height

    # Наносим текст
    drawing_object.text((x, y), watermark_text, fill=(255, 255, 255, 125), font=font)

    # Соединяем изначальное изображение и наш слой с ватермаркой. Меняем мод нашего изображения на RGBA
    # иначе ничего работать не будет (A - Alpha нужна для задания прозрачности)
    try:
        image = image.convert("RGBA")
        return Image.alpha_composite(image, layer_to_draw)
    except Exception as err:
        print(f'Exception watermarking testImages.py:\n{err}')


def image_changer(image_path: str,
                  new_image_path: str,
                  new_height: int = 0,
                  new_width: int = 0,
                  affect_it: bool = False,
                  draw_date: bool = False,
                  watermark_it: bool = False) -> bool:
    try:
        image_root, image_ext = os.path.splitext(new_image_path)
        with Image.open(str(image_path)) as image:
            # Меняем размер
            resizing(image, new_height, new_width)
            # накладываем эффект
            if affect_it:
                image = affecting(image)
            # наносим дату
            if draw_date:
                date_drawing(image)
            # наносим ватермарку
            if watermark_it:
                image = watermarking(image)
                image_ext = '.png'
            try:
                image.save(image_root + image_ext)
                return True
            except Exception as err:
                print(f'Can\'t save a finished file. Exception from image_changer function in testImages.py:\n{err}')
                return False
    except Exception as err:
        print(f'Exception from image_changer in testImages.py:\n{err}')


def generate_random_digits(nums_from: int, nums_to: int, count: int) -> str:
    """Generates an id with (range of numbers from 'nums_from' to 'nums_to') times 'count' symbols in it.
    Output is going to be a string"""
    digits = ''.join((str(randint(nums_from, nums_to)).zfill(len(str(nums_to))) for _ in range(count)))
    return digits


if __name__ == '__main__':
    print('Endpoint is in the checker.py')
