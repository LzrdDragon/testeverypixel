import time
import pathlib
import os

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

import testImages
import input_interface


class Watcher:
    DIRECTORY_TO_WATCH = str(pathlib.Path(__file__).parent.resolve()) + f'{os.sep}dir_to_get_imgs_from'

    def __init__(self):
        self.observer = Observer()

    def run(self) -> None:
        event_handler = Handler()
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except Exception as err:
            self.observer.stop()
            print(f'An error from Watcher in checker.py:\n{err}')

        self.observer.join()


class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event) -> None:

        if event.is_directory:
            return None

        elif event.event_type == 'created':
            directory_to_save_to = str(pathlib.Path(__file__).parent.resolve()) + f'{os.sep}dir_to_store_imgs_to{os.sep}'
            img_name = str(event.src_path).split(f'{os.sep}')[-1]
            save_to = directory_to_save_to + img_name
            print("Received created event - %s." % event.src_path)
            print('Starting to process it...')
            result = testImages.image_changer(
                image_path=event.src_path,
                new_height=new_height,
                new_width=new_width,
                new_image_path=save_to,
                affect_it=affect_it,
                draw_date=draw_date,
                watermark_it=watermark_it)
            if result:
                print('Just finished, it saved to ./dir_to_store_imgs_to')
                try:
                    os.remove(event.src_path)
                    print('source file removed')
                except Exception as err:
                    print(f'\nAn exception due to removing a file in Handler class in checker.py:'
                          f'\n{err}'
                          f'\nFilename:{event.src_path}\n')

        # elif event.event_type == 'modified':
        #     print("Received modified event - %s." % event.src_path)


if __name__ == '__main__':
    new_height, new_width, affect_it, draw_date, watermark_it = input_interface.terminal_input_user_interface()
    w = Watcher()
    w.run()
