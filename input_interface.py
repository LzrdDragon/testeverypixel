import time


def terminal_input_user_interface() -> list:
    target_parameters = []
    user_language = input('If you want to continue in English, please type in "en"'
                          '\nЕсли вы хотите продолжить на Русском, введите "ru": ')
    user_attempt = 0
    while True:
        if user_language == 'en':
            print('Hello, notice that images will be saved in "dir_to_store_imgs_to" directory')
            time.sleep(3)

            while True:
                try:
                    user_height = int(input('If you want to change image height, '
                                            'enter an integer number of it, else enter 0: '))
                    target_parameters.append(user_height)
                except Exception as err:
                    print(f'There was an exception in terminal_input_user_interface function of input_data.py:'
                          f'\n{err}')
                else:
                    break

            while True:
                try:
                    user_width = int(input('If you want to change image width, '
                                           'enter an integer number of it, else enter 0: '))
                    target_parameters.append(user_width)
                except Exception as err:
                    print(f'There was an exception in terminal_input_user_interface function of input_data.py:'
                          f'\n{err}')
                else:
                    break

            while True:
                try:
                    affecting_choice = bool(int(input('If you want to blur an image enter 1. If not, enter 0: ')))
                    target_parameters.append(affecting_choice)
                except Exception as err:
                    print(f'There was an exception in terminal_input_user_interface function of input_data.py:'
                          f'\n{err}')
                else:
                    break

            while True:
                try:
                    draw_date_choice = bool(int(input('If you want to draw date on an image enter 1. If not, enter 0: ')))
                    target_parameters.append(draw_date_choice)
                except Exception as err:
                    print(f'There was an exception in terminal_input_user_interface function of input_data.py:'
                          f'\n{err}')
                else:
                    break

            while True:
                try:
                    watermarking_choice = bool(int(input('If you want to watermark an image enter 1. Else enter 0'
                                                         '\nNotice that extension of files will be always'
                                                         ' .png in that case: ')))
                    target_parameters.append(watermarking_choice)
                except Exception as err:
                    print(f'There was an exception in terminal_input_user_interface function of input_data.py:'
                          f'\n{err}')
                else:
                    break

            break

        elif user_language == 'ru':
            print('Привет, обратите внимание, что изображения будут сохранены в директории "dir_to_store_imgs_to"')
            time.sleep(3)

            while True:
                try:
                    user_height = int(input('Если вы хотите менять высоту изображений, введите целое число желаемой'
                                            ' высоты, если нет, пожалуйста введите 0: '))
                    target_parameters.append(user_height)
                except Exception as err:
                    print(f'There was an exception in terminal_input_user_interface function of input_data.py:'
                          f'\n{err}')
                else:
                    break

            while True:
                try:
                    user_width = int(input('Если вы хотите менять ширину изображений, введите целое число желаемой'
                                           ' ширины, если нет, пожалуйста введите 0: '))
                    target_parameters.append(user_width)
                except Exception as err:
                    print(f'There was an exception in terminal_input_user_interface function of input_data.py:'
                          f'\n{err}')
                else:
                    break

            while True:
                try:
                    affecting_choice = bool(int(input('Если вы хотите размывать изображения, '
                                                      'введите 1, если нет, введите 0: ')))
                    target_parameters.append(affecting_choice)
                except Exception as err:
                    print(f'There was an exception in terminal_input_user_interface function of input_data.py:'
                          f'\n{err}')
                else:
                    break

            while True:
                try:
                    draw_date_choice = bool(int(input('Наносить дату их попадания в директорию\n'
                                                      'на изображения?\n'
                                                      '1 - Да'
                                                      '\n0 - Нет: ')))
                    target_parameters.append(draw_date_choice)
                except Exception as err:
                    print(f'There was an exception in terminal_input_user_interface function of input_data.py:'
                          f'\n{err}')
                else:
                    break

            while True:
                try:
                    watermarking_choice = bool(int(input('Наносить ватермарку?\n1 - да\n0 - нет'
                                                         '\nЗаметьте, что расширение файлов в таком случае '
                                                         'всегда будет .png\n: ')))
                    target_parameters.append(watermarking_choice)
                except Exception as err:
                    print(f'There was an exception in terminal_input_user_interface function of input_data.py:'
                          f'\n{err}')
                else:
                    break

            break

        else:
            user_attempt += 1
            if user_attempt == 5:
                user_language = input('只需输入"en"或"ru": ')
            else:
                user_language = input('Em? Just - en'
                                      '\nНе понял вас, просто - ru: ')
    return target_parameters


if __name__ == '__main__':
    print('Endpoint is in the checker.py')
